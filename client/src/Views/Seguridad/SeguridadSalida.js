import React, { Component } from 'react';
import './estilos.css';
import NavSeguridad from '../../Components/Navs/NavSeguridad/NavSeguridad';
import {TitleIng } from '../../Components/common/Texts';
import NuevaSalida from '../../Components/Forms/Salida/NuevaSalida'


class Admin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rol: '',
            openModal: false,
        }
    }
    render() {
        return ( 
            <div className='containerSeguridad'>
                <NavSeguridad></NavSeguridad><br/><br/><br/><br/><br/><br/>
                <TitleIng titleing='VERIFICAR SALIDA' />
                <br/>
                    <div className=''>
                        <NuevaSalida />
                    </div> 
            </div>
        )
    }
}

export default Admin