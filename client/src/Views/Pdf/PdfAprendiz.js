import React, { Component } from 'react'
import { Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';

const PdfAprendiz = props => {
    console.log(props.apr)

    const BORDER_COLOR = '#bfbfbf'
    const BORDER_STYLE = 'solid'
    const COL1_WIDTH = 10
    const COLN_WIDTH = (49 - COL1_WIDTH) / 3

    const styles = StyleSheet.create({
        body: {
            padding: 10
        },
        table: {
            display: "table",
            width: "auto",
            borderStyle: BORDER_STYLE,
            borderColor: BORDER_COLOR,
            borderWidth: 1,
            borderRightWidth: 0,
            borderBottomWidth: 0
        },
        tableRow: {
            margin: 0,
            flexDirection: "row"
        },
        tableCol1Header: {
            width: COL1_WIDTH + '%',
            borderStyle: BORDER_STYLE,
            borderColor: BORDER_COLOR,
            borderBottomColor: '#000',
            borderWidth: 1,
            borderLeftWidth: 0,
            borderTopWidth: 0
        },
        tableColHeader: {
            width: COLN_WIDTH + "%",
            borderStyle: BORDER_STYLE,
            borderColor: BORDER_COLOR,
            borderBottomColor: '#000',
            borderWidth: 1,
            borderLeftWidth: 0,
            borderTopWidth: 0
        },
        tableCol1: {
            width: COL1_WIDTH + '%',
            borderStyle: BORDER_STYLE,
            borderColor: BORDER_COLOR,
            borderWidth: 1,
            borderLeftWidth: 0,
            borderTopWidth: 0
        },
        tableCol: {
            width: COLN_WIDTH + "%",
            borderStyle: BORDER_STYLE,
            borderColor: BORDER_COLOR,
            borderWidth: 1,
            borderLeftWidth: 0,
            borderTopWidth: 0
        },
        tableCellHeader: {
            margin: 5,
            fontSize: 12,
            fontWeight: 500
        },
        tableCell: {
            margin: 5,
            fontSize: 8
        },

        titulo: {
            textAlign: "center",
            fontWeight: "bold",
            fontSize: 32,
            marginBottom: 40
        }
    });

    return (
        <Document>
            <Page size="A4" style={styles.body}>
                <View>
                    <Text style={styles.titulo}>Aprendiz</Text>
                </View>

                <View style={styles.tableRow}>
                    <View style={styles.tableCol1Header}>
                        <Text style={styles.tableCellHeader}>Nombre</Text>
                    </View>
                    <View style={styles.tableColHeader}>
                        <Text style={styles.tableCellHeader}>Email</Text>
                    </View>
                    <View style={styles.tableColHeader}>
                        <Text style={styles.tableCellHeader}>Documento identidad</Text>
                    </View>
                    <View style={styles.tableColHeader}>
                        <Text style={styles.tableCellHeader}>Telefono</Text>
                    </View>
                    <View style={styles.tableColHeader}>
                        <Text style={styles.tableCellHeader}>Direccion</Text>
                    </View>
                    <View style={styles.tableColHeader}>
                        <Text style={styles.tableCellHeader}>Eps</Text>
                    </View>
                    <View style={styles.tableColHeader}>
                        <Text style={styles.tableCellHeader}>Ficha</Text>
                    </View>
                    <View style={styles.tableColHeader}>
                        <Text style={styles.tableCellHeader}>Programa</Text>
                    </View>
                </View>

                {props.apr ?
                    props.apr.map((a, index) =>{
                        return(
                            <View key={index} style={styles.tableRow}>

                                    <View style={styles.tableCol1}>
                                        <Text style={styles.tableCell} > {a.nombre} </Text>
                                    </View>

                                    <View style={styles.tableCol}>
                                        <Text style={styles.tableCell}> {a.email} </Text>
                                    </View>

                                    <View style={styles.tableCol}>
                                        <Text style={styles.tableCell}> {a.documentoIdentidad} </Text>
                                    </View>

                                    <View style={styles.tableCol}>
                                        <Text style={styles.tableCell}> {a.telefono} </Text>
                                    </View>

                                    <View style={styles.tableCol}>
                                        <Text style={styles.tableCell}> {a.ficha} </Text>
                                    </View>

                                    <View style={styles.tableCol}>
                                        <Text style={styles.tableCell}> {a.eps} </Text>
                                    </View>

                                    <View style={styles.tableCol}>
                                        <Text style={styles.tableCell}> {a.ficha} </Text>
                                    </View>

                                    <View style={styles.tableCol}>
                                        <Text style={styles.tableCell}> {a.programaDeFormacion} </Text>
                                    </View>

                                </View>
                        )
                    })

                : ''}

            </Page>
        </Document>
    )

}

export default PdfAprendiz;