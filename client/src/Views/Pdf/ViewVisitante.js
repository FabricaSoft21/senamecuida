import React, { Component } from 'react';

import NavAdmin from '../../Components/Navs/NavAdmin';

import { PDFViewer, PDFDownloadLink } from '@react-pdf/renderer';
import PdfVisitante from './PdfVisitante'

import axios from 'axios'



class ViewVisitante extends Component {

    state = {
        visitantes: []
    }

    getVisitantes = async () => {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/pdf/visitante`)
        this.setState({ visitantes: res.data })
    }

    async componentDidMount() {
        this.getVisitantes()
        // console.log(this.state.users)
    }


    render() {
        // console.log(this.state.visitantes)
        return (
            <div className='containerAdmin' style={{ minHeight: "100vh" }}>

                <NavAdmin></NavAdmin>

                visitantes

                <PDFViewer style={{ width: "100%", height: "90vh" }} >
                    <PdfVisitante visi={this.state.visitantes} />
                </PDFViewer>


            </div>
        )
    }
}

export default ViewVisitante
