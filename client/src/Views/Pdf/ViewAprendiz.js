import React, { Component } from 'react';

import NavAdmin from '../../Components/Navs/NavAdmin';

import { PDFViewer, PDFDownloadLink } from '@react-pdf/renderer';
import PdfAprendices from './PdfAprendiz'

import axios from 'axios'


class ViewAprendiz extends Component {

    state = {
        aprendices: []
    }

    getAprendices = async () => {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/pdf/aprendiz`)
        this.setState({ aprendices: res.data })
    }

    async componentDidMount() {
        this.getAprendices()
        // console.log(this.state.users)
    }

    

    render() {
        // console.log(this.state.aprendices)
        return (
            <div className='containerAdmin' style={{ minHeight: "100vh" }}>

                <NavAdmin></NavAdmin>

                aprendiz

                <PDFViewer style={{ width: "100%", height: "90vh" }} >
                    <PdfAprendices apr={this.state.aprendices} />
                </PDFViewer>                
                
            </div>
        )
    }
}

export default ViewAprendiz
