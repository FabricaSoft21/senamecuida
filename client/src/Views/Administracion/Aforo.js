//creacion: 7/12/2021
//modificacion: 20/01/2022 
import React, { useState,Component } from "react";
import NavAdmin from '../../Components/Navs/NavAdmin';
import {Title} from "../../Components/common/Texts";
import './estilos.css';
import Swal from 'sweetalert2';
import TextField from '@material-ui/core/TextField';
import {makeStyles} from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { components } from "react-select";
//import {DatePicker} from '@material-ui/pickers';
//import 'react-datepicker/dist/react-datepicker.css';


const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const Aforo = () => {

    const classes = useStyles();
    const [fechaI, setFechaI] = React.useState('')
    const [fechaF, setFechaF] = React.useState('')
    const [torre, setTorre] = React.useState('')
    const [piso, setPiso] = React.useState('')
    const [cantidad, setCantidad] = React.useState('')

   
    const handleFechaIChange = (event) => setFechaI(event.target.value)
    const handleFechaFChange = (event) => setFechaF(event.target.value)
    const handleTorreChange = (event) => setTorre(event.target.value)
    const handlePisoChange = (event) => setPiso(event.target.value)
    const handleCantidadChange = (event) => setCantidad(event.target.value)

    if(fechaF < fechaI){
        Swal.fire({
            title: 'Error!',
            text: 'La fecha final debe ser mayor a la fecha de inicio',    
        })
           
}
  
    
    return (
        <div className='containerForm'>
              <NavAdmin></NavAdmin>
                <Title title='AFORO'/>
                
            <div style={{ height:'50%', width: '50%', marginLeft: '1%' }}>  
            <FormControl variant="outlined" fullWidth className={classes.formControl}>
          <label>Fecha de inicio</label>
                <TextField 
                      value={fechaI}
                      onChange={handleFechaIChange}
                      required
                      name="fechaI"
                      id='fechaI'
                      type='date'
                      placeholder='Ingresa una fecha '
                      variant="outlined"
                />
                </FormControl>
           
            </div>

            <div style={{ width: '50%', marginLeft: '1%' }}>
            <FormControl variant="outlined" fullWidth className={classes.formControl}>
            <label>Fecha Final</label>
                <TextField 
                      value={fechaF}
                      onChange={handleFechaFChange}
                      required
                      name="fechaF"
                      id='fechaF'
                      type='date'
                      placeholder='Ingresa una fecha limite'
                      variant="outlined"
                />
                </FormControl>
            </div>
      
            <div style={{ width: '50%', marginLeft: '1%' }}>
                <FormControl variant="outlined" fullWidth className={classes.formControl}>
                <label>Torre</label>
                    <InputLabel id="demo-simple-select-outlined-label"></InputLabel>
                    <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        label="torre"
                        value={torre}
                        onChange={handleTorreChange}
                    >
                        <MenuItem value={'Torre Sur'} onChange={handleTorreChange}>Torre Sur</MenuItem>
                        <MenuItem value={'Torre Norte'} onChange={handleTorreChange}>Torre Norte</MenuItem>
                    </Select>
                </FormControl>
            </div>
            <div style={{ width: '50%', marginLeft: '1%' }}>
                <FormControl variant="outlined" fullWidth className={classes.formControl}>
                    <label>N° piso</label>
                    <InputLabel id="demo-simple-select-outlined-label"></InputLabel>
                    <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        label=""
                        value={piso}
                        onChange={handlePisoChange}
                    >
                        <MenuItem value={'1'} onChange={handlePisoChange}>1</MenuItem>
                        <MenuItem value={'2'} onChange={handlePisoChange}>2</MenuItem>
                        <MenuItem value={'3'} onChange={handlePisoChange}>3</MenuItem>
                        <MenuItem value={'4'} onChange={handlePisoChange}>4</MenuItem>
                        <MenuItem value={'5'} onChange={handlePisoChange}>5</MenuItem>
                        <MenuItem value={'6'} onChange={handlePisoChange}>6</MenuItem>
                        <MenuItem value={'7'} onChange={handlePisoChange}>7</MenuItem>
                        <MenuItem value={'8'} onChange={handlePisoChange}>8</MenuItem>
                        <MenuItem value={'9'} onChange={handlePisoChange}>9</MenuItem>
                        <MenuItem value={'10'} onChange={handlePisoChange}>10</MenuItem>
                        <MenuItem value={'Sótano'} onChange={handlePisoChange}>Sótano</MenuItem>
                    </Select>
                </FormControl>
            </div>
            <div style={{width: '50%', marginLeft: '1%'}}>
            <FormControl variant="outlined" fullWidth className={classes.formControl}>
            <label>Cantidad de personas</label>
                <TextField
                    value={15}
                   // onChange={handleCantidadChange}
                    //required
                    //name="cantidad"
                    //id='cantidad'
                    //type=''
                    //placeholder=''
                    variant="outlined"
                    readOnly
                />
                </FormControl>
            </div>
        </div>

        );
    }
    


export default Aforo;
