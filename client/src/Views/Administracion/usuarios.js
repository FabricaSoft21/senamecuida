import React, { Component } from 'react';


import './estilos.css';
import NavAdmin from '../../Components/Navs/NavAdmin';
//import Table from '../../Components/Table/Table';
import TableAprendiz from '../../Components/Table/TableAprendiz/Table';
import TableFuncionaro from '../../Components/Table/TableFuncionario/Table';
import TableVisitante from '../../Components/Table/TableVisitante/Table';
// import { Card, CardInfo } from '../../Components/Cards/Cards';
import CardInfoAprendiz from '../../Components/Cards/CardAprendiz/CardInfoAprendiz';
import CardInfoVisitante from '../../Components/Cards/CardVisitante/CardInfoVisitante';
import CardInfoFuncionario from '../../Components/Cards/CardFuncionario/CardInfoFuncionario';
// import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { Title } from '../../Components/common/Texts';

import { BrowserRouter as Router, Route, Link } from 'react-router-dom'







class Admin extends Component {

    state = {
        visitantes:[],
        funcionarios: [],
        aprendices: []
    }

    visitante = e =>{
        document.getElementById('visitante').style.display = 'block'
        document.getElementById('funcionario').style.display = 'none'
        document.getElementById('aprendices').style.display = 'none'
        
        document.getElementById('btn-visitante').style.display = 'block'
        document.getElementById('btn-funcionario').style.display = 'none'
        document.getElementById('btn-aprendiz').style.display = 'none'
    }

    funcionario = e =>{
        document.getElementById('funcionario').style.display = 'block'
        document.getElementById('visitante').style.display = 'none'
        document.getElementById('aprendices').style.display = 'none'

        document.getElementById('btn-funcionario').style.display = 'block'
        document.getElementById('btn-visitante').style.display = 'none'
        document.getElementById('btn-aprendiz').style.display = 'none'
    }

    aprendiz = e =>{
        document.getElementById('aprendices').style.display = 'block'
        document.getElementById('visitante').style.display = 'none'
        document.getElementById('funcionario').style.display = 'none'

        document.getElementById('btn-aprendiz').style.display = 'block'
        document.getElementById('btn-visitante').style.display = 'none'
        document.getElementById('btn-funcionario').style.display = 'none'
    }

    render() {
        return (
            <div className='containerAdmin'>

                <NavAdmin></NavAdmin>

                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <input type="button" class="btn btn-sm btn-outline-secondary" value="Visitantes" onClick={this.visitante}/>
                            </li>
                            <li class="nav-item">
                                <input type="button" class="btn btn-sm btn-outline-secondary" value="Funcionarios" onClick={this.funcionario}/>
                            </li>
                            <li class="nav-item">
                                <input type="button" class="btn btn-sm btn-outline-secondary" value="Aprendices" onClick={this.aprendiz}/>
                            </li>
                            <li class="nav-item">
                                <Link class="btn btn-sm btn-outline-secondary" id="btn-visitante" to="/pdfvisitante" >Informe Visitante</Link>
                            </li>
                            <li class="nav-item">
                                <Link class="btn btn-sm btn-outline-secondary" id="btn-funcionario" to="/pdffuncionario" >Informe Funcionario</Link>
                            </li>
                            <li class="nav-item">
                                <Link class="btn btn-sm btn-outline-secondary" id="btn-aprendiz" to="/pdfaprendiz">Informe Aprendiz</Link>
                            </li>
                        </ul>
                    </div>
                </nav>
                    <div id="visitante">
                        <div class="container">
                            <Title title='VISITANTES REGISTRADOS' />
                                <div className='contCardsInfo' class="alert alert-light"  >
                                    <CardInfoVisitante  />
                                </div>
                            <div className='contAdmin1'>
                                <div className='contEstadisticas'>
                                    <TableVisitante />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="funcionario" >
                        <div class="container">
                            <Title title='FUNCIONARIOS REGISTRADOS' />
                            <div className='contCardsInfo' class="alert alert-light"  >
                                    <CardInfoFuncionario />
                                </div>
                            <div className='contAdmin1'>
                                <div className='contEstadisticas'>
                                    <TableFuncionaro />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="aprendices" >
                        <div class="container">
                            <Title title='APRENDICES REGISTRADOS' />
                            <div className='contCardsInfo' class="alert alert-light"  >
                                    <CardInfoAprendiz  />
                                </div>
                            <div className='contAdmin1'>
                                <div className='contEstadisticas'>
                                    <TableAprendiz />
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        )
    }
}

export default Admin
