import React from 'react';
import './estilos.css';
import Swal from 'sweetalert2';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { Modal } from 'react-bootstrap';
import { Title } from '../../Components/common/Texts';
import { Form, Container, Row, Col, Button } from 'react-bootstrap'
import ReCAPTCHA from "react-google-recaptcha";

// import { Input } from '../common/Inputs';
import { ButtonIcon } from '../../Components/common/Button';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const Visitante = () => {

    const classes = useStyles();
    const [nombre, setNombre] = React.useState('')
    const [email, setEmail] = React.useState('')
    const [documentoIdentidad, setDocumentoIdentidad] = React.useState('')
    const [celular, setCelular] = React.useState('')
    const [telefono, setTelefono] = React.useState('')
    const [direccionResidencia, setDireccionResidencia] = React.useState('')
    const [eps, setEps] = React.useState('')
    const [torre, setTorre] = React.useState("")
    const [piso, setPiso] = React.useState("")
    const [sexo, setSexo] = React.useState("")
    const [transporte, setTransporte] = React.useState("")

    const handleNombreChange = (event) => setNombre(event.target.value)
    const handleEmailChange = (event) => setEmail(event.target.value)
    const handleDocumentoIdentidadChange = (event) => setDocumentoIdentidad(event.target.value)
    const handleCelularChange = (event) => setCelular(event.target.value)
    const handleTelefonoChange = (event) => setTelefono(event.target.value)
    const handleDireccionResidenciaChange = (event) => setDireccionResidencia(event.target.value)
    const handleEpsChange = (event) => setEps(event.target.value)
    const handleSexoChange = (event) => setSexo(event.target.value)

    function prevent() {
        document.querySelector("#numeroId").addEventListener("keypress", function (evt) {
            if (evt.which !== 8 && evt.which !== 0 && evt.which < 48 || evt.which > 57) {
                evt.preventDefault();
            }
        });
        document.querySelector("#tel1").addEventListener("keypress", function (evt) {
            if (evt.which !== 8 && evt.which !== 0 && evt.which < 48 || evt.which > 57) {
                evt.preventDefault();
            }
        });
        document.querySelector("#celular").addEventListener("keypress", function (evt) {
            if (evt.which !== 8 && evt.which !== 0 && evt.which < 48 || evt.which > 57) {
                evt.preventDefault();
            }
        });
        document.querySelector("#nombre").addEventListener("keypress", function (evt) {
            if (!(evt.which >= 65 && evt.which <= 122) && (evt.which !== 32 && evt.which !== 0) && !(evt.which === 241)) {
                evt.preventDefault();
            }
        });
        var NID = document.querySelector('#numeroId');
        NID.addEventListener('input', function () {
            if (this.value.length > 10)
                this.value = this.value.slice(0, 10);
        })
        var NTEL = document.querySelector('#tel1');
        NTEL.addEventListener('input', function () {
            if (this.value.length > 10)
                this.value = this.value.slice(0, 10);
        })
        var NTELF = document.querySelector('#celular');
        NTELF.addEventListener('input', function () {
            if (this.value.length > 10)
                this.value = this.value.slice(0, 10);
        })
    }
    


    async function registroE(valores) {
        let sintomas = valores
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var address = document.querySelector('#correo').value;
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var address = document.querySelector('#correo').value;
        var name = document.querySelector('#nombre').value;
        var ID = document.querySelector('#numeroId').value;
        var tel = document.querySelector('#tel1').value;
        var cel = document.querySelector('#celular').value;
        var direccion = document.querySelector('#direccion').value;
        if (reg.test(address) == false) {
            Swal.fire({
                icon: 'error',
                title: '¡Error!',
                text: "¡Debes ingresar una direccion de correo electronico valida!",
                timer: 10500
            })
            return (false);
        } else if (name.length < 8) {
            Swal.fire({
                icon: 'error',
                title: '¡Error!',
                text: "¡Debes ingresar tu nombre completo!",
                timer: 10500
            })
            return (false);
        } else if (ID.length < 5) {
            Swal.fire({
                icon: 'error',
                title: '¡Error!',
                text: "¡Debes ingresar un numero de documento de identidad valido!",
                timer: 10500
            })
            return (false);
        } else if (tel.length < 7) {
            Swal.fire({
                icon: 'error',
                title: '¡Error!',
                text: "¡Debes ingresar un numero de telefono valido!",
                timer: 10500
            })
            return (false);
        } else if (cel.length < 7) {
            Swal.fire({
                icon: 'error',
                title: '¡Error!',
                text: "¡Debes ingresar otro numero de telefono valido!",
                timer: 10500
            })
            return (false);
        } else if (direccion.length < 10) {
            Swal.fire({
                icon: 'error',
                title: '¡Error!',
                text: "¡Debes ingresar una dirección valida!",
                timer: 10500
            })
            return (false);
        } else {
            await fetch(`${process.env.REACT_APP_API_URL}/api/visitante/create`, {
                // await fetch(`${process.env.REACT_APP_API_URL}/api/visitante/create`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',

                },
                body: JSON.stringify({
                    nombre,
                    sexo,
                    email,
                    documentoIdentidad,
                    celular,
                    telefono,
                    direccionResidencia,
                    eps,
                })
            }).then(function (result) {
                if (result['ok'] === true) {
                    result.text().then(function (data) {
                        Swal.fire({
                            icon: 'success',
                            title: '¡BIEN!',
                            text: data,
                            timer: 1500
                        })
                        setTimeout(() => {
                            window.location.reload();
                        }, 5000);
                    })
                } else if (result.status === 400) {
                    result.text().then(function (data) {
                        Swal.fire({
                            icon: 'error',
                            title: '¡DEBES LLENAR TODOS LOS CAMPOS!',
                            timer: 10500
                        })
                    })
                } else {
                    result.text().then(function (data) {
                        Swal.fire({
                            icon: 'error',
                            title: '¡Este dato ya se encuentra registrado en el aplicativo!',
                            text: data,
                            timer: 10500
                        })
                    })
                }

            })
                .catch(function (error) {
                    console.log(error)
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: error,
                        timer: 1500
                    })
                });
        }
    }

    return (
        <div className='containerForm'>
            <Form onSubmit={registroE}>
            <TextField
                value={nombre}
                onChange={handleNombreChange}
                onKeyDown={prevent}
                required
                fullWidth
                name="nombre"
                id='nombre'
                type='text'
                label='Nombre completo'
                placeholder='Ingresa tu nombre completo'
                variant="outlined"
            />
            <div style={{ width: '100%', marginTop: '1%', marginLeft: '-2%' }}>
                <FormControl variant="outlined" fullWidth className={classes.formControl}>
                    <InputLabel id="demo-simple-select-outlined-label">Genero</InputLabel>
                    <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        label="SEXO"
                        value={sexo}
                        onChange={handleSexoChange}
                    >
                        <MenuItem value={'Masculino'} onChange={handleSexoChange}>Masculino</MenuItem>
                        <MenuItem value={'Femenino'} onChange={handleSexoChange}>Femenino</MenuItem>
                        <MenuItem value={'Prefiero no decir'} onChange={handleSexoChange}>Prefiero no decir</MenuItem>
                    </Select>
                </FormControl>
            </div>
            <div style={{ width: '100%', marginTop: '1%' }}>
                <TextField
                    value={email}
                    onChange={handleEmailChange}
                    required
                    fullWidth
                    name="email"
                    id='correo'
                    type='email'
                    label='Correo electrónico'
                    placeholder='Ingresa tu correo'
                    variant="outlined"
                />
            </div>
            <div style={{ width: '100%', marginTop: '1.5%' }}>
                <TextField
                    value={documentoIdentidad}
                    onChange={handleDocumentoIdentidadChange}
                    onKeyDown={prevent}
                    required
                    fullWidth
                    name="documentoIdentidad"
                    id='numeroId'
                    type='number'
                    label='Número de documento'
                    placeholder='Ingresa tu número de documento'
                    variant="outlined"
                />
            </div>
            <div style={{ width: '100%', marginTop: '1.5%' }}>
                <TextField
                    value={telefono}
                    onChange={handleTelefonoChange}
                    onKeyDown={prevent}
                    required
                    fullWidth
                    name="telefono"
                    id='tel1'
                    type='number'
                    label='Número de teléfono'
                    placeholder='Ingresa tu número de teléfono'
                    variant="outlined"
                />
            </div>
            <div style={{ width: '100%', marginTop: '1.5%' }}>
                <TextField
                    value={celular}
                    onChange={handleCelularChange}
                    onKeyDown={prevent}
                    required
                    fullWidth
                    name="celular"
                    id='celular'
                    type='number'
                    label='Número de teléfono de un familiar'
                    placeholder='Ingresa número de teléfono'
                    variant="outlined"
                />
            </div>
            <div style={{ width: '100%', marginTop: '1.5%' }}>
                <TextField
                    value={direccionResidencia}
                    onChange={handleDireccionResidenciaChange}
                    required
                    fullWidth
                    name="direccionResidencia"
                    id='direccion'
                    type='text'
                    label='Dirección'
                    placeholder='Ingresa tu dirección de residencia'
                    variant="outlined"
                />
            </div>
            <div style={{ width: '100%', marginTop: '1%', marginLeft: '-2%' }}>
                <FormControl variant="outlined" fullWidth className={classes.formControl}>
                    <InputLabel id="demo-simple-select-outlined-label">EPS</InputLabel>
                    <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        label="EPS"
                        value={eps}
                        onChange={handleEpsChange}
                    >
                        <MenuItem value={'NINGUNA'} onChange={handleEpsChange}>NINGUNA</MenuItem>
                        <MenuItem value={'SURA'} onChange={handleEpsChange}>SURA</MenuItem>
                        <MenuItem value={'Coomeva'} onChange={handleEpsChange}>Coomeva</MenuItem>
                        <MenuItem value={'Salud Colmena'} onChange={handleEpsChange}>Salud Colmena</MenuItem>
                        <MenuItem value={'Salud total'} onChange={handleEpsChange}>Salud Total</MenuItem>
                        <MenuItem value={'Cafesalud'} onChange={handleEpsChange}>Cafesalud</MenuItem>
                        <MenuItem value={'Sanitas'} onChange={handleEpsChange}>Sanitas</MenuItem>
                        <MenuItem value={'Medimas'} onChange={handleEpsChange}>Medimas</MenuItem>
                        <MenuItem value={'Colseguros'} onChange={handleEpsChange}>Colseguros</MenuItem>
                        <MenuItem value={'Colpatria'} onChange={handleEpsChange}>Colpatria</MenuItem>
                        <MenuItem value={'Cruz blanca'} onChange={handleEpsChange}>Cruz Blanca</MenuItem>
                        <MenuItem value={'Sisben'} onChange={handleEpsChange}>Sisben</MenuItem>
                        <MenuItem value={'Sanidad Militar'} onChange={handleEpsChange}>Sanidad Militar</MenuItem>
                        <MenuItem value={'Red Vital'} onChange={handleEpsChange}>Red Vital</MenuItem>
                        <MenuItem value={'Nueva EPS'} onChange={handleEpsChange}>Nueva EPS</MenuItem>
                        <MenuItem value={'SaviaSalud'} onChange={handleEpsChange}>SaviaSalud</MenuItem>
                    </Select>
                </FormControl>
            </div>

            <div style={{marginTop: 25}}>
                <ButtonIcon
                    bgColor='#00A7AF'
                    title='Registrar'
                    onClick={() => registroE()}
                />
            </div>
            </Form>
        </div>

    )
}

export default Visitante;
