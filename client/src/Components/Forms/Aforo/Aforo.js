import React, { useEffect } from 'react';
import './estilos.css';
import { Form, Container, Row, Col, Button } from 'react-bootstrap'
import Swal from 'sweetalert2';
import Checkbox from '@material-ui/core/Checkbox';
import { withStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import { Modal } from 'react-bootstrap';

// Torres

import { TorreNorte, TorreSur } from '../Torre/Torres'




const GreenCheckbox = withStyles({
    root: {
        color: green[50],
        '&$checked': {
            color: green[50],
        },
    },
    checked: {},
})((props) => <Checkbox  {...props} />);


const Aforo = (props) => {

    


    const [documentoIdentidad, setDocumentoIdentidad] = React.useState('');
    const [dataState, setDataState] = React.useState({});


    const [transporte, setTransporte] = React.useState("")


    const changeTransporte = (e) => {
        var transporte = document.getElementById('transporte').value
        console.log(transporte);
        setTransporte(transporte)

    



    }

    //cuerpo del reporte
    return (
        <div>
            <center>
                <h1>Selecciona el medio en el que llegaste y el lugar al que te diriges</h1>
            </center>
            <div>
                <div className="">
                    <Container>
                        <div id="lateral">
                            <Form >
                                <Row className='row-reporte'>
                                    <Row>
                                        <Form.Label>
                                            <strong>Medio de transporte</strong>
                                        </Form.Label>
                                        <br></br>
                                        <select id="transporte" name="transporte" onChange={() => { changeTransporte() }}>
                                            <option selected value="caminando">Caminando</option>
                                            <option value="automovilparticular">Automovil PARTICULAR</option>
                                            <option value="bicicleta">Bicicleta</option>
                                            <option value="transportepublico">transporte PUBLICO</option>
                                        </select>
                                    </Row>
                                </Row>

                                <Row>
                                    <Form.Label>
                                        <center>
                                            <strong>Hacia donde te diriges</strong>
                                        </center>
                                        <br></br>
                                    </Form.Label>


                                    <br></br>
                                    <div id="torres">

                                        <button className="btn btn-primary btn-block" type="button" name="torres" value="norte" onClick={() => {

                                            document.getElementById('norte').style.display = 'block'
                                            document.getElementById('sur').style.display = 'none'
                                        }}>Torre norte</button>
                                    </div>
                                    <div id="torres">
                                        <button className="btn btn-primary btn-block" type="button" name="torres" value="sur" onClick={() => {

                                            document.getElementById('sur').style.display = 'block'
                                            document.getElementById('norte').style.display = 'none'
                                        }}>Torre Sur</button>
                                    </div>
                                </Row>
                                <Row>
                                    <div id="norte">
                                        <TorreNorte
                                            torre="norte"
                                            cedula={props.cedula}
                                            usuario={props.usuario}
                                            transporte={transporte}
                                        />
                                    </div>
                                    <div id="sur">
                                        <TorreSur
                                            torre="sur"
                                            cedula={props.cedula}
                                            usuario={props.usuario}
                                            transporte={transporte}
                                        />
                                    </div>
                                </Row>
                            </Form>
                        </div>
                    </Container>
                </div>
            </div>
        </div>

    )
}


export default Aforo;