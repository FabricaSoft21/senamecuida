
import TextField from '@material-ui/core/TextField';
import { Form, Container, Row, Col, Alert, Button } from 'react-bootstrap'
import Swal from 'sweetalert2';
import axios from 'axios';
import { async } from 'rxjs';
import './estilos.css';
import React, { useEffect } from 'react';
import Aforo from '../Aforo/Aforo';
import { Modal } from 'react-bootstrap';
import { ButtonIcon } from '../../common/Button';

const EncuentaCovid = (props) => {


    const [modalAforo, setModalAforo] = React.useState(false);
    const OpenModalAforo = () => setModalAforo(true)
    const CloseModalAforo = () => setModalAforo(false)




    const [fiebre, setFiebre] = React.useState(false)
    const [dolorTragar, setDolorTragar] = React.useState(false)
    const [Tos, setTos] = React.useState(false)
    const [dificultadRespirar, setDificultadRespirar] = React.useState(false)
    const [malestargeneral, setMalestarGeneral] = React.useState(false)
    const [gripa, setGripa] = React.useState(false)
    const [diarrea, setDiarrea] = React.useState(false)
    const [contacto, setContacto] = React.useState(false)
    const [tratamiento, setTratamiento] = React.useState(false)


    const [documentoIdentidad, setDocumentoIdentidad] = React.useState('');
    const [dataState, setDataState] = React.useState({});

    const [dosis, setDosis] = React.useState('');


    const handleDocumentoIdentidadChange = (e) => {
        console.log(e.target.value);
        setDocumentoIdentidad(e.target.value);
    };

    const changeDosis = (e) => {
        var dosis = document.getElementById('dosis').value
        setDosis(dosis)
    }




    function prevent() {
        document.querySelector("#documentoIdentidad").addEventListener("keypress", function (evt) {
            if (evt.which !== 8 && evt.which !== 0 && evt.which < 48 || evt.which > 57) {
                evt.preventDefault();
            }
        });
        var NID = document.querySelector('#documentoIdentidad');
        NID.addEventListener('input', function () {
            if (this.value.length > 10)
                this.value = this.value.slice(0, 10);
        });
    }

    useEffect(() => {

        const callSearchService = () => {

            if (documentoIdentidad !== '') {
                registro();
            }
        };

        let consultarAPI = setTimeout(() => {
            callSearchService();
        }, 3500);

        // Se dispara cada vez que se re-renderiza el componente
        return () => {
            clearTimeout(consultarAPI);
        };
    }, [documentoIdentidad]);

    async function registro() {
        await fetch(`${process.env.REACT_APP_API_URL}/api/aprendiz/ingresoAf`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ documentoIdentidad })
        })
            .then(function (result) {
                if (result['ok'] === true) {
                    result.text().then(function (data) {
                        setDataState(data);
                        console.log(data);
                    })
                        .then(
                            fetch(`${process.env.REACT_APP_API_URL}/api/ingresoSuspendido/ing`, {
                                method: 'POST',
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify({ documentoIdentidad })
                            })
                                .then(function (result) {
                                    if (result['ok'] === true) {
                                        Swal.fire({
                                            icon: 'error',
                                            title: '¡BLOQUEADO!',
                                            showConfirmButton: false,
                                            text: '¡Debes contactar al medico SENA por el ultimo reporte que dice que tienes mas de 3 sintomas!',
                                            timer: 10500
                                        });
                                    } else {
                                        result.text().then(function (data) {
                                            Swal.fire({
                                                icon: 'success',
                                                title: `¡ENCONTRADO!`,
                                                text: "Seleccione sus sintomas",
                                                showConfirmButton: false,
                                                timer: 10500
                                            });
                                        });
                                    }
                                })
                        );
                } else {
                    result.text().then(function (data) {
                        Swal.fire({
                            icon: 'error',
                            title: '¡ERROR!',
                            text: data,
                            timer: 10500
                        });

                    });
                }
            });


    }

    // const Enviar = async (resultados,cedula, dosis) => {

    //     console.log(resultados,cedula, dosis)

    //     await axios.post(`${process.env.REACT_APP_API_URL}/api/email/covid`, {
    //         cedula: cedula,
    //         resultado: resultados
    //     })

    //     await axios.post(`${process.env.REACT_APP_API_URL}/api/email/covidAp`, {
    //         cedula: cedula,
    //         resultado: resultados
    //     })

    //     await axios.post(`${process.env.REACT_APP_API_URL}/api/encuestas/covid`, {
    //         cedula: cedula,
    //         resultado: resultados
    //     })
    // }



    const alertaAcceso = (e) => {
        Swal.fire({
            title: 'No presentas sintomas',
            confirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Continuar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                OpenModalAforo()
            }
        })
    }

    // const alertaAccesoDenegado = (e) =>{
    //     Swal.fire({
    //         title: 'Presentas sintomas',
    //         text: 'Se enviara un correo al enfermero',
    //         timer: 3000,
    //         timerProgressBar: true,
    //         showConfirmButton: false
    //     }).then(()=>{
    //         Enviar(e, documentoIdentidad, dosis)
    //         window.location.replace('');
    //     })
    // }

    const validar = async e => {


        let si = 0
        let no = 0

        const data = [
            "fiebre",
            "Tos",
            "dolorTragar",
            "malestargeneral",
            "dificultadRespirar",
            "gripa",
            "diarrea",
            "contacto",
            "tratamiento",
        ]

        const sintomas = data.reduce(
            (out, bool, index) => bool ? out.concat(index) : out,
            []
        )

        if (sintomas.length >= 3) {
            Swal.fire({
                icon: 'error',
                title: '¡No cumple con las reglas!',
                text: 'No puede pasar!',
                timer: 10500
            })
        } else {
            Swal.fire({
                icon: 'success',
                title: '¡Cumple con las reglas!',
                text: 'Puede pasar!',
                timer: 10500
            })
        }

        let resultados = []


        data.forEach((item, index) => {
            let check = document.querySelectorAll(`[name="${item}"]`)

            check.forEach((e) => {
                if (e.checked) {
                    if (e.value === "si") {
                        // console.log("si")
                        resultados.push(e.value)
                        si++
                    } else {
                        // console.log("no")
                        resultados.push(e.value)
                        no++
                    }
                }
            })
        })

        if (dosis == 'Ninguna') {
            // alertaAccesoDenegado(resultados)
        } else {
            if (no > si) {
                alertaAcceso()
            } else {
                // alertaAccesoDenegado(resultados)
            }
        }



    }

    return (
        <div>
            <div className="card-body">
                <Container>
                    <h3>¿Presenta algunos de estos sintomas sintomas?</h3>
                    <div>
                        <div className='containerForm'>
                            <TextField
                                value={documentoIdentidad}
                                onChange={handleDocumentoIdentidadChange}
                                onKeyDown={() => prevent()}
                                name="documentoIdentidad"
                                required
                                id='documentoIdentidad'
                                type='number'
                                label='Documento de Identidad'
                                placeholder='Ingresa el documento de identidad'
                                variant='outlined'
                                style={{ marginBottom: '5%' }}
                            />
                        </div>
                    </div>
                    <hr />
                    <Form >
                        <Row className='row-reporte'>

                            <Col>
                                <Form.Label>
                                    <strong>Fiebre?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setFiebre(e.target.value = true)}
                                    value={fiebre} name={'fiebre'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setFiebre(e.target.value = false)}
                                    value={fiebre} name={'fiebre'} label={'No'} value="no" />
                            </Col>
                            <Col>
                                <Form.Label>
                                    <strong>Tos?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setTos(e.target.value = true)}
                                    value={Tos} name={'Tos'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setTos(e.target.value = false)}
                                    value={Tos} name={'Tos'} label={'No'} value="no" />
                            </Col>
                            <Col>
                                <Form.Label>
                                    <strong>Dolor al tragar?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setDolorTragar(e.target.value = true)}
                                    value={dolorTragar} name={'dolorTragar'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setDolorTragar(e.target.value = false)}
                                    value={dolorTragar} name={'dolorTragar'} label={'No'} value="no" />
                            </Col>
                        </Row>
                        <Row className='row-reporte'>
                            <Col>
                                <Form.Label>
                                    <strong>Malestar general?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setMalestarGeneral(e.target.value = true)}
                                    value={malestargeneral} name={'malestargeneral'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setMalestarGeneral(e.target.value = false)}
                                    value={malestargeneral} name={'malestargeneral'} label={'No'} value="no" />
                            </Col>
                            <Col>
                                <Form.Label>
                                    <strong>Dificultad para respirar?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setDificultadRespirar(e.target.value = true)}
                                    value={dificultadRespirar} name={'dificultadRespirar'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setDificultadRespirar(e.target.value = false)}
                                    value={dificultadRespirar} name={'dificultadRespirar'} label={'No'} value="no" />

                            </Col>
                            <Col>
                                <Form.Label>
                                    <strong>Gripa?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setGripa(e.target.value = true)}
                                    value={gripa} name={'gripa'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setGripa(e.target.value = false)}
                                    value={gripa} name={'gripa'} label={'No'} value="no" />
                            </Col>
                        </Row>
                        <Row className='row-reporte'>
                            <Col>
                                <Form.Label>
                                    <strong>Diarrea?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setDiarrea(e.target.value = true)}
                                    value={diarrea} name={'diarrea'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setDiarrea(e.target.value = false)}
                                    value={diarrea} name={'diarrea'} label={'No'} value="no" />
                            </Col>
                            <Col>
                                <Form.Label>
                                    <strong>A tenido contacto con casos sospechosos o confirmados?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setContacto(e.target.value = true)}
                                    value={contacto} name={'contacto'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setContacto(e.target.value = false)}
                                    value={contacto} name={'contacto'} label={'No'} value="no" />

                            </Col>
                            <Col>
                                <Form.Label>
                                    <strong>Se encuentra en tratamiento por enfermedad actual S?</strong>
                                </Form.Label>
                                <Form.Check type="radio" onChange={e => setTratamiento(e.target.value = true)}
                                    value={tratamiento} name={'tratamiento'} label={'Si'} value="si" />
                                <Form.Check type="radio" onChange={e => setTratamiento(e.target.value = false)}
                                    value={tratamiento} name={'tratamiento'} label={'No'} value="no" />
                            </Col>
                        </Row>
                        <div className='containerForm'>

                            <Row className='row-reporte'>

                                <Form.Label>
                                    <strong>¿Cuantas dosis de la vacuna tiene?</strong>

                                    <Col>
                                        <select id="dosis" name="dosis" onChange={() => { changeDosis() }}>
                                            <option selected value="seleccion">Elegir</option>
                                            <option value="Ninguna">Ninguna</option>
                                            <option value="Dosis1">1 Dosis</option>
                                            <option value="Dosis2">2 Dosis</option>
                                            <option value="Dosis3">3 Dosis</option>
                                        </select>
                                    </Col>
                                </Form.Label>
                            </Row>

                        </div>


                        <Row>
                            <Modal show={modalAforo} onHide={CloseModalAforo} backdrop="static"
                                keyboard={false}>

                                <Modal.Header>
                                </Modal.Header>
                                <Modal.Body>
                                    <Aforo
                                        usuario={dataState}
                                        cedula={documentoIdentidad}
                                    />
                                </Modal.Body>
                                <Modal.Footer>
                                    <ButtonIcon bgColor='#00A7AF' title='Anterior' onClick={() => {
                                        setModalAforo(false);
                                    }} />
                                </Modal.Footer>
                            </Modal>
                            <div className="App">
                                <button type="button" name="" class="btn btn-success"  onClick={() => validar()}>Siguiente</button>
                            </div>
                        </Row>
                    </Form>
                </Container>
            </div>
        </div>
    )
}
export default EncuentaCovid