import React, { Component } from 'react'
import './estilos.css';
import { Modal } from 'react-bootstrap';
import { ButtonIcon } from '../../../Components/common/Button';
import ModalQr from './Modalqr';
import { async } from 'rxjs';
import axios from 'axios'

const Enviar = (props) => {

    const [modalQr, setModalQr] = React.useState(false);
    const OpenModalQr = () => setModalQr(true);
    const CloseModalQr = () => setModalQr(false);

    console.log(props.pisos);

    const guardar = async e =>{
        axios.post(`${process.env.REACT_APP_API_URL}/api/aprendiz/ingresoAf`,{

        })
    }


    return (
        <div>

            <input type="button" value="Enviar" class="btn btn-success" onClick={() => {
                OpenModalQr();
            }} />
            <Modal
                backdrop="static"
                keyboard={false}
                show={modalQr}
                onHide={CloseModalQr}>
                <Modal.Header>
                </Modal.Header>
                <Modal.Body>
                    <ModalQr 
                        torre={props.usuario + " , " + props.torre + " , " + props.transporte}
                    />
                </Modal.Body>
                <Modal.Footer>
                    <ButtonIcon bgColor='#e74c3c' title='Cerrar' onClick={() => {
                        window.location.replace('');
                    }} />
                </Modal.Footer>
            </Modal>
        </div>

    )


}

export default Enviar;