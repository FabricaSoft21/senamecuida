import React, { Component } from 'react'
import './estilos.css';
import Enviar from './Enviar';

let pisos = ''

const piso = e => {

    if (e) {
        let salones = document.getElementById(e)

        if (salones.classList.contains('invisible')) {

            salones.classList.replace('invisible', 'visible')
            pisos = e


        } else if (salones.classList.contains('visible')) {

            salones.classList.replace('visible', 'invisible')
        }
    }

}

const salon = e =>{
    if(e){
        let salon = document.getElementById(e)
        
    }
    
}



const TorreNorte = (props) => {

    console.log(props.torre);
    console.log(props.cedula);
    console.log(props.usuario);
    console.log(props.transporte);

    console.log(pisos);

    return (
        <div>
            <div class="list-group">

                <div class="">
                    <button type="button" class="list-group-item list-group-item-action" onClick={() => { piso('piso1') }} value="piso1">Piso 1 norte</button>
                    <div id="piso1" class="invisible">
                        <button type="button" class="list-group-item list-group-item-action" /* onClick={() => { salon('salon1') }} */>salon 1</button>
                        <button type="button" class="list-group-item list-group-item-action" /* onClick={() => { salon('salon2') }} */>salon 2</button>
                    </div>
                </div>

                <div class="">
                    <button type="button" class="list-group-item list-group-item-action" onClick={() => { piso('salonesN2') }} value="piso1">Piso 2 norte</button>
                    <div id="salonesN2" class="invisible">
                        <button type="button" class="list-group-item list-group-item-action" /* onClick={() => { salon('salon1') }} */>salon 1</button>
                        <button type="button" class="list-group-item list-group-item-action" /* onClick={() => { salon('salon2') }} */>salon 2</button>
                    </div>
                </div>

            </div>

            <div id="" class="">
                <Enviar
                    piso={pisos}
                    torre={props.torre}
                    cedula={props.cedula}
                    usuario={props.usuario}
                    transporte={props.transporte}
                />
            </div>

        </div>
    )
}




const TorreSur = (props) => {

    console.log(props.torre);
    console.log(props.cedula);
    console.log(props.usuario);
    console.log(props.transporte);

    return (
        <div >
            <div class="list-group">

                <div class="">
                    <button type="button" class="list-group-item list-group-item-action" onClick={() => { piso('salonesS1') }} value="piso1">Piso 1 sur</button>
                    <div id="salonesS1" class="invisible">
                        <button type="button" class="list-group-item list-group-item-action" /* onClick={() => { salon('salon1') }} */>salon 1</button>
                        <button type="button" class="list-group-item list-group-item-action"/*  onClick={() => { salon('salon2') }} */>salon 2</button>
                    </div>
                </div>

                <div class="">
                    <button type="button" class="list-group-item list-group-item-action" onClick={() => { piso('salonesS2') }} value="piso1">Piso 2 sur</button>
                    <div id="salonesS2" class="invisible">
                        <button type="button" class="list-group-item list-group-item-action" /* onClick={() => { salon('salon1') }} */>salon 1</button>
                        <button type="button" class="list-group-item list-group-item-action"/*  onClick={() => { salon('salon2') }} */>salon 2</button>
                    </div>
                </div>

            </div>
            <div id="" class="">
                <Enviar
                    torre={props.torre}
                    cedula={props.cedula}
                    usuario={props.usuario}
                    transporte={props.transporte}
                />
            </div>
        </div>
    )
}


export { TorreNorte, TorreSur }