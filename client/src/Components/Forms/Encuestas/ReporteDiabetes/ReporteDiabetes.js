import React from 'react';
import { Form, Container, Row, Col} from 'react-bootstrap'
import { Modal } from 'react-bootstrap';
import { Title } from '../../../common/Texts';
import Swal from 'sweetalert2';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { withStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import { ButtonIcon } from '../../../common/Button';
import axios from 'axios';

const GreenCheckbox = withStyles({
    root: {
        color: green[50],
        '&$checked': {
            color: green[50],
        },
    },
    checked: {},
})((props) => <Checkbox color="default" {...props} />);

const ReporteDiabetes = (props) => {
    const mostrarAlerta1=()=>{
        Swal.fire({
          title: 'Seleccionaste todas tus respuestas?',
          showDenyButton: true,
          showCancelButton: true,
          confirmButtonText: `Si, enviar`,
          cancelButtonText: `No, responder`,
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            enviar()
            Swal.fire({
                title: '¡Genial!',
                text: 'Gracias por confirmar tus sintomas y llenar la encuesta',
                confirmButtonText: 'Siguiente',
                allowOutsideClick: false,
                allowEscapeKey: false
            }).then((result)=>{
                if(result.isConfirmed){
                    Swal.fire({
                        title: 'Confirmacion',
                        text: 'Enviamos una pequeña recomendacion tomando en cuenta tus respuestas',
                        timer: 3000,
                        timerProgressBar: true,
                        showConfirmButton: false
                    }).then(()=>{
                        window.location.replace('');
                    })
                }
            })

          } else if (result.isDenied) {
            Swal.fire('Selecciona los sintomas que presentas', '', 'info')
          }
        });
      }

    const [modalPreguntas, setModalPreguntas] = React.useState(false);
    const [contadorPreguntas, setContadorPreguntas] = React.useState(0);
    const OpenModalPreguntas = () => setModalPreguntas(true);
    const CloseModalPreguntas = () => setModalPreguntas(false);

    const [state, setState] = React.useState({
        checked2: false,
        checked3: false,
        checked4: false,
        checked5: false,
        checked6: false,
    });

    const handleChange = (event) => {
        event.preventDefault();
        setState({ ...state, [event.target.name]: event.target.checked });
        if (event.target.checked) {
            setContadorPreguntas(contadorPreguntas + 1);
        } else {
            setContadorPreguntas(contadorPreguntas - 1);
        }
        
        console.log(state)
    };

    const contarChecks = (event) => {
        if ((state.checked2  == true && state.checked3 == true && state.checked4 == true) || 
            (state.checked2  == true && state.checked3 == true && state.checked6 == true) ||
            (state.checked2  == true && state.checked3 == true && state.checked5 == true) ||
            (state.checked2  == true && state.checked4 == true && state.checked6 == true) ||
            (state.checked2  == true && state.checked5 == true && state.checked4 == true) ||
            (state.checked5  == true && state.checked6 == true && state.checked4 == true) ||
            (state.checked3  == true && state.checked4 == true && state.checked6 == true) 
            ) {
            Swal.fire({
                title: 'Desea llenar una encuesta de sintomas relacionados a la diabetes?',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: `SI`,
                denyButtonText: `NO`,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    OpenModalPreguntas()
                }
            })
        } else {
            Swal.fire('Usted no presenta sintomas relacionados con diabetes.');
            setTimeout(() => {
                window.location.replace('');
            }, 5000);
        }

    };

    const cedula = props.cedula;

    const enviar = async e => {
        const data = [
            "presion",
            "embarazo",
            "colesterol",
            "glucosa",
            "cardiaco",
            "diabetes"
        ]

        const sintomas = [
            "checked2",
            "checked3",
            "checked4",
            "checked5",
            "checked6",
        ]

        let resultados = []
        let rSintomas = []
    
        data.forEach((item, index) => {
            let check = document.querySelectorAll(`[name="${item}"]`)

            check.forEach((e) => {
                if (e.checked) {
                    if (e.value === "si") {
                        // console.log("si")
                        resultados.push(e.value)
                    } else {
                        // console.log("no")
                        resultados.push(e.value)
                    }
                }
            })
        })

        sintomas.forEach((item, index) => {
            let check = document.getElementById(`${item}`)

            // console.log(check)

            if(check.checked==true){
                rSintomas.push(check.value)
            }else{
                console.log('nanada')
            }

        })

        resultados.forEach((item, index) => {
            console.log(item, index)
        })

        rSintomas.forEach((item, index) => {
            console.log(`aca el nombre  ${item}`)
        })

        // email
    
        await axios.post(`${process.env.REACT_APP_API_URL}/api/email/diabetes`, {
            cedula: cedula,
            resultado: resultados
        })


        await axios.post(`${process.env.REACT_APP_API_URL}/api/email/diabetesAp`, {
            cedula: cedula,
            resultado: resultados,
            sintoma: rSintomas
        })

        //base de datos

        await axios.post(`${process.env.REACT_APP_API_URL}/api/encuestas/diabetes`, {
            cedula: cedula,
            resultado: resultados
        })

    }

    return (
        <div className='containerForm'>
            <div>
                <div className="card-body">
                    <Container>
                        <div className='div-h3'>
                            <h3>Selecciona 5 sintomas para continuar</h3>
                        </div>
                        <Form >
                            <Row>
                                <FormGroup row>
                                    <h1><span role="img" aria-label="inodoro" >🚽</span></h1>
                                    <FormControlLabel
                                        control={
                                            <GreenCheckbox
                                                checked={state.checked1}
                                                onChange={handleChange}
                                                name="checked1"
                                                color="primary"
                                                disabled={state.checked1 === true ? false : contadorPreguntas === 5 ? true : false}

                                            />
                                        }
                                    />
                                    <h5>Indigestión</h5>
                                </FormGroup>
                                <FormGroup row>
                                    <h1><span role="img" aria-label="hambre" >🤤</span></h1>
                                    <FormControlLabel
                                        control={
                                            <GreenCheckbox
                                                checked={state.checked2}
                                                onChange={handleChange}
                                                name="checked2"
                                                id="checked2"
                                                color="primary"
                                                disabled={state.checked2 === true ? false : contadorPreguntas === 5 ? true : false}
                                                value="checked2"

                                            />
                                        }
                                    />
                                    <h5>Hambre extrema</h5>
                                </FormGroup>
                                <FormGroup row>
                                    <h1><span role="img" aria-label="pesa" >⚖️</span></h1>
                                    <FormControlLabel
                                        control={
                                            <GreenCheckbox
                                                checked={state.checked3}
                                                onChange={handleChange}
                                                name="checked3"
                                                id="checked3"
                                                color="primary"
                                                disabled={state.checked3 === true ? false : contadorPreguntas === 5 ? true : false}
                                                value="checked3"
                                            />
                                        }
                                    />
                                    <h5>Perdida de peso</h5>
                                </FormGroup>
                            </Row>
                            <hr />
                            <Row>
                                <FormGroup row>
                                    <h1><span role="img" aria-label="fatiga" >😪</span></h1>
                                    <FormControlLabel
                                        control={
                                            <GreenCheckbox
                                                checked={state.checked4}
                                                onChange={handleChange}
                                                name="checked4"
                                                id="checked4"
                                                color="primary"
                                                disabled={state.checked4 === true ? false : contadorPreguntas === 5 ? true : false}
                                                value="checked4"

                                            />
                                        }
                                    />
                                    <h5>Fatiga</h5>
                                </FormGroup>
                                <FormGroup row>
                                    <h1><span role="img" aria-label="irritable" >😤</span></h1>
                                    <FormControlLabel
                                        control={
                                            <GreenCheckbox
                                                checked={state.checked5}
                                                onChange={handleChange}
                                                name="checked5"
                                                id="checked5"
                                                color="primary"
                                                disabled={state.checked5 === true ? false : contadorPreguntas === 5 ? true : false}
                                                value="checked5"
                                            />
                                        }
                                    />
                                    <h5>Enojo</h5>
                                </FormGroup>
                                <FormGroup row>
                                    <h1><span role="img" aria-label="ojos" >👀</span></h1>
                                    <FormControlLabel
                                        control={
                                            <GreenCheckbox
                                                checked={state.checked6}
                                                onChange={handleChange}
                                                name="checked6"
                                                id="checked6"
                                                color="primary"
                                                disabled={state.checked6 === true ? false : contadorPreguntas === 5 ? true : false}
                                                value="checked6"
                                            />
                                        }
                                    />
                                    <h5>Visión borrosa</h5>
                                </FormGroup>
                            </Row>
                            <hr />
                            <Row >
                                <FormGroup row>
                                    <h1><span role="img" aria-label="diente" >🦷</span></h1>
                                    <FormControlLabel
                                        control={
                                            <GreenCheckbox
                                                checked={state.checked7}
                                                onChange={handleChange}
                                                name="checked7"
                                                color="primary"
                                                disabled={state.checked7 === true ? false : contadorPreguntas === 5 ? true : false}

                                            />
                                        }
                                    />
                                    <h5>Infección oral</h5>
                                </FormGroup>
                                <FormGroup row>
                                    <h1><span role="img" aria-label="sed" >🥛</span></h1>
                                    <FormControlLabel
                                        control={
                                            <GreenCheckbox
                                                checked={state.checked8}
                                                onChange={handleChange}
                                                name="checked8"
                                                color="primary"
                                                disabled={state.checked8 === true ? false : contadorPreguntas === 5 ? true : false}

                                            />
                                        }
                                    />
                                    <h5>Sed</h5>
                                </FormGroup>

                                <FormGroup row>
                                    <h1><span role="img" aria-label="estornudos" >🤧</span></h1>
                                    <FormControlLabel
                                        control={
                                            <GreenCheckbox
                                                checked={state.checked9}
                                                onChange={handleChange}
                                                name="checked9"
                                                color="primary"
                                                disabled={state.checked9 === true ? false : contadorPreguntas === 5 ? true : false}

                                            />
                                        }
                                    />
                                    <h5>Estornudo</h5>
                                </FormGroup>
                            </Row>
                            <hr />
                        </Form>
                        <ButtonIcon
                            bgColor='#00A7AF'
                            title='Continuar'
                            onClick={contarChecks}
                        />
                    </Container>
                    
                    <Modal show={modalPreguntas} onHide={CloseModalPreguntas}>
                        <Modal.Header>
                            <Modal.Title>
                                <Title title='FORMULARIO  DE  SINTOMAS' />
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Container>
                                <h2>Ayudanos a conocer tu estado de salud de posible DIABETES <span role="img" aria-label="feliz" >😄</span></h2>
                                <h3>Marca SI los sintomas que presentas</h3>
                                <hr />
                                <Form onSubmit={enviar} >
                                    <Row className='row-reporte'>
                                        <Col>
                                            <Form.Label>
                                                <strong>Tiene la presión arterial alta?</strong>
                                            </Form.Label>
                                            <Form.Check id="presion" type="radio"
                                                name={'presion'} label={'Si'} value="si" />
                                            <Form.Check id="presion" type="radio"
                                                name={'presion'} label={'No'} value="no" />
                                        </Col>
                                        <Col>
                                            <Form.Label>
                                                <strong>Estas en estado de embarazo?</strong>
                                            </Form.Label>
                                            <Form.Check id="embarazo" type="radio"
                                                name={'embarazo'} label={'Si'} value="si" />
                                            <Form.Check id="embarazo" type="radio"
                                                name={'embarazo'} label={'No'} value="no" />
                                        </Col>
                                        <Col>
                                            <Form.Label>
                                                <strong>Presentas niveles anormales de colesterol?</strong>
                                            </Form.Label>
                                            <Form.Check id="colesterol" type="radio"
                                                name={'colesterol'} label={'Si'} value="si" />
                                            <Form.Check id="colesterol" type="radio"
                                                name={'colesterol'} label={'No'} value="no" />
                                        </Col>

                                    </Row>
                                    <hr />
                                    <Row className='row-reporte'>
                                        <Col>
                                            <Form.Label>
                                                <strong>Te has realizado algun examen de glucosa?</strong>
                                            </Form.Label>
                                            <Form.Check id="glucosa" type="radio"
                                                name={'glucosa'} label={'Si'} value="si" />
                                            <Form.Check id="glucosa" type="radio"
                                                name={'glucosa'} label={'No'} value="no" />
                                        </Col>
                                        <Col>
                                            <Form.Label>
                                                <strong>Tienes antecedentes de síndrome de ovario poliquístico o enfermedad cardíaca?</strong>
                                            </Form.Label>
                                            <Form.Check id="cardiaco" type="radio"
                                                name={'cardiaco'} label={'Si'} value="si" />
                                            <Form.Check id="cardiaco" type="radio"
                                                name={'cardiaco'} label={'No'} value="no" />
                                        </Col>
                                        <Col>
                                            <Form.Label>
                                                <strong>Tienes algun pariente cercano con diabetes?</strong>
                                            </Form.Label>
                                            <Form.Check id="diabetes" type="radio"
                                                name={'diabetes'} label={'Si'} value="si" />
                                            <Form.Check id="diabetes" type="radio"
                                                name={'diabetes'} label={'No'} value="no" />
                                        </Col>
                                    </Row>
                                    <hr />
                                    <div className="App">
                                        <button type="button" class="btn btn-success" onClick={() => mostrarAlerta1()}>prueba</button>
                                    </div>
                                </Form>
                            </Container>
                        </Modal.Body>
                        <Modal.Footer>
                            <ButtonIcon bgColor='#00A7AF' title='Anterior' onClick={() => {
                                setModalPreguntas(false)
                            }} />
                            <ButtonIcon bgColor='#e74c3c' title='Cerrar' onClick={() => {
                                setModalPreguntas(false)
                            }} />
                        </Modal.Footer>
                    </Modal>
                </div>
            </div>
        </div>
    )
}
export default ReporteDiabetes;