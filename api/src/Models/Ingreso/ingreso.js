'use strict'
require('mongoose-type-email');
const { Schema, model } = require("mongoose");
require('mongoose-type-email');


const ingresoSchema = new Schema({
    cedula: {
        type: String,
        required: true,
        max: 30
    },
    nombre: {
        type: String,
        required: true,
        max: 30
    },
    celular: {
        type: String,
        required: false,
        max: 30
    },
    dosis: {
        type: String,
        required: true,
        max: 50
    },
    torre: {
        type: String,
        required: true,
        max: 20,
    },
    piso: {
        type: String,
        required: true,
        max: 50
    },
    salon: {
        type: String,
        required: true,
        max: 50
    },
    sintomas: {
        type: String,
        required: true,
        max: 50
    },
},{
    timestamps: true
})

module.exports = model("Ingreso", ingresoSchema);

