'use strict'
require('mongoose-type-email');
const { Schema, model } = require("mongoose");
const ttl = require('mongoose-ttl');
require('mongoose-type-email');
const moment = require('moment');


const DiabetesSchema = new Schema({
    documentoIdentidad:{
        type:String,
        required: true,
        max: 15
    },
    presionArterial: {
        type: String,
        required: true,
        max: 5
    },
    embarazo: {
        type: String,
        required: true,
        max: 5
    },
    colesterol: {
        type: String,
        required: true,
        max: 5
    },
    exameGlucosa: {
        type: String,
        required: true,
        max: 5
    },
    antecedentes: {
        type: String,
        required: true,
        max: 5
    },
    pariente: {
        type: String,
        required: true,
        max: 5
    }
},{
    timestamps: true
})

module.exports = model("Diabetes", DiabetesSchema);