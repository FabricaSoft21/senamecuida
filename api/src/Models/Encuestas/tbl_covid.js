'use strict'
require('mongoose-type-email');
const { Schema, model } = require("mongoose");
const ttl = require('mongoose-ttl');
require('mongoose-type-email');
const moment = require('moment');


const CovidSchema = new Schema({
    documentoIdentidad:{
        type:String,
        required: true,
        max: 15
    },
    fiebre: {
        type: String,
        required: true,
        max: 5
    },
    tos: {
        type: String,
        required: true,
        max: 5
    },
    dolorTragar: {
        type: String,
        required: true,
        max: 5
    },
    malestarGeneral: {
        type: String,
        required: true,
        max: 5
    },
    dificultadRespirar: {
        type: String,
        required: true,
        max: 5
    },
    gripa: {
        type: String,
        required: true,
        max: 5
    },
    diarrea: {
        type: String,
        required: true,
        max: 5
    },
    contactoSospechoso: {
        type: String,
        required: true,
        max: 5
    },
    tratamiento: {
        type: String,
        required: true,
        max: 5
    },
    
},{
    timestamps: true
})

module.exports = model("Covid", CovidSchema);