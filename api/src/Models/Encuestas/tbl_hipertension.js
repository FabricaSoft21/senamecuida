'use strict'
require('mongoose-type-email');
const { Schema, model } = require("mongoose");
const ttl = require('mongoose-ttl');
require('mongoose-type-email');
const moment = require('moment');


const HipertesionSchema = new Schema({
    documentoIdentidad:{
        type:String,
        required: true,
        max: 15
    },
    ejercicio: {
        type: String,
        required: true,
        max: 30
    },

    presionarterial: {

        type: String,
        required: true,
        max: 5
    },

    diagnosticodiabetes: {
        type: String,
        required: true,
        max: 5
    },

    familiar: {
        type: String,
        required: true,
        max: 5
    }
},{
    timestamps: true
})

module.exports = model("Hipertension", HipertesionSchema);