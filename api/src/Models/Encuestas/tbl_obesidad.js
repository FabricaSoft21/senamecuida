'use strict'
require('mongoose-type-email');
const { Schema, model } = require("mongoose");
const ttl = require('mongoose-ttl');
require('mongoose-type-email');
const moment = require('moment');


const ObesidadSchema = new Schema({
    documentoIdentidad:{
        type:String,
        required: true,
        max: 15
    },
    peso: {
        type: String,
        required: true,
        max: 10,
    },

    estatura: {
        type: String,
        required: true,
        max: 10,
    },

    ejercicio: {
        type: String,
        required: true,
        max: 30
    },

    comidachatarra: {
        type: String,
        required: true,
        max: 30
    },

    familiar: {
        type: String,
        required: true,
        max: 5
    },

    dieta: {
        type: String,
        required: true,
        max: 5
    }
}, {
    timestamps: true
})

module.exports = model("Obesidad", ObesidadSchema);