'use strict'
require('mongoose-type-email');
const { Schema, model } = require("mongoose");
const ttl = require('mongoose-ttl');
require('mongoose-type-email');
const moment = require('moment');


const DepresionSchema = new Schema({
    documentoIdentidad:{
        type:String,
        required: true,
        max: 15
    },
    perdidainteres: {
        type: String,
        required: true,
        max: 5
    },
    altibajos: {
        type: String,
        required: true,
        max: 5
    },
    faltaconcentracion: {
        type: String,
        required: true,
        max: 5
    },
    malpeso: {
        type: String,
        required: true,
        max: 5
    },
    somnolencia: {
        type: String,
        required: true,
        max: 5
    },
    aislamiento: {
        type: String,
        required: true,
    }
},{
    timestamps: true
})

module.exports = model("Depresion", DepresionSchema);