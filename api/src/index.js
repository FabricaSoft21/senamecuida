require("./Configs/config");
require("./Configs/database");
console.clear();

// ---------------------------------- //
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan')
const path = require('path')
const cors = require('cors')
var helmet = require('helmet');

//sentry CI/CD process and validate issues 
//const express = require('express');
const Sentry = require('@sentry/node');
const Tracing = require("@sentry/tracing");

// Initialize the application
const app = express();
app.disable('x-powered-by');

//static files
app.use('/exports', express.static(path.resolve('exports')))

// Middlewares
app.use(helmet());
app.use('/static', express.static(__dirname + '/reportes'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('dev'))
app.set('trust proxy', true);

// Cors
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});


//metodo CI / CD integracion start services 
Sentry.init({
    dsn: "https://45114742bffc4f4189314057c06940bb@o1089877.ingest.sentry.io/6172243",
    integrations: [
      // enable HTTP calls tracing
      new Sentry.Integrations.Http({ tracing: true }),
      // enable Express.js middleware tracing
      new Tracing.Integrations.Express({ app }),
    ],
  
    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
  });


// Habilitar cors (DE MANERA LIMITADA)
// En esta ocasion sera abierta para mi
/*const whitelist=['https://senamecuida.sertec-csge.com.co'];
const corsOptions={
    origin: (origin,callback) => {

        const existe = whitelist.some(dominio => dominio=== origin);

        if (existe) {
            callback(null,true)
        } else {
            callback(new Error('No permitido por CORS'))
        }
    }
}*/

// Esta limita el acceso
// app.use(cors(corsOptions));

// Esta es abierta para todo el mundo.
app.use(cors());

// RequestHandler creates a separate execution context using domains, so that every
// transaction/span/breadcrumb is attached to its own Hub instance
app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());


//Routes
app.use("/api/visitante", require('./Routes/VisitantesRoutes/visitanteRoutes'));
app.use("/api/funcionario", require('./Routes/FuncionarioRoutes/funcionarioRoutes'));
app.use("/api/aprendiz", require('./Routes/AprendizRoutes/aprendizRoutes'));
app.use("/api/administrador", require('./Routes/AdministradorRoutes/administradorRoutes'))
app.use("/api/seguridad", require('./Routes/Seguridad/seguridadRoutes'))
app.use("/api/ingresoDia", require('./Routes/IngresoRoutes/ingresoDiaRoutes'))
app.use("/api/salidaDia", require('./Routes/SalidaRoutes/salidaDiaRoutes'))
app.use("/api/estado", require('./Routes/Estado/estadoRoutes'))
app.use("/api/estadoAprendiz", require('./Routes/AprendizRoutes/estadoAprendizRoutes'))
app.use("/api/estadoFuncionario", require('./Routes/FuncionarioRoutes/estadoFuncionarioRoutes'))
app.use("/api/estadoVisitante", require('./Routes/VisitantesRoutes/estadoVisitanteRoutes'))
app.use("/api/noIngresoDia", require('./Routes/IngresoRoutes/noIngresoDiaRoutes'))
app.use("/api/saludEstado", require('./Routes/Estado/EstadoSaludRoutes'))
app.use("/api/reporteSaludDia", require('./Routes/ReportesRoutes/reporteSaludDiaRoutes'))
app.use("/api/reporteSalud", require('./Routes/ReportesRoutes/reporteSaludRoutes'))
app.use("/api/reporteInsumo", require('./Routes/ReportesRoutes/repInsumoRoutes'))
app.use("/api/reporteLimpieza", require('./Routes/ReportesRoutes/repLimpiezaRoutes'))
app.use("/api/ingresoSuspendido", require('./Routes/IngresoRoutes/ingresoSuspendidoRoutes'))
app.use("/api/soporte", require('./Routes/SoporteRoutes/soporteRoute'))
app.use("/api/habilitar", require('./Routes/HabilitarUsuarios/habilitarRoutes'))
app.use("/api/csv", require('./Routes/ExportarRoutes/exportarRoutes'))


//  Enviar email
app.use("/api/email", require('./Routes/Email/enviarEmailRoutes'))

// guardar encuestas
app.use("/api/encuestas", require('./Routes/Ecuestas/guardarEncuestas'))

// guardar ingreso
app.use("/api/ingreso", require('./Routes/IngresoRoutes/guardarIngreso'))


//PDF
app.use("/api/pdf", require('./Routes/Pdf/pdf'))


// The error handler must be before any other error middleware and after all controllers
app.use(Sentry.Handlers.errorHandler());

// Optional fallthrough error handler
app.use(function onError(err, req, res, next) {
  // The error id is attached to `res.sentry` to be returned
  // and optionally displayed to the user for support.
  res.statusCode = 500;
  res.end(res.sentry + "\n");
});

// Run the server
app.listen(process.env.PORT, () => {
    console.log(`Servidor corriendo en el puerto ${process.env.PORT}`);
});
