//libs
const convertir = require('json-2-csv')
const fs = require('fs')
//Modelos
const Covid = require('../../Models/Encuestas/tbl_covid')
const Hipertension = require('../../Models/Encuestas/tbl_hipertension')
const Obesidad = require('../../Models/Encuestas/tbl_obesidad')
const Depresion = require('../../Models/Encuestas/tbl_depresion')
const Diabetes = require('../../Models/Encuestas/tbl_diabetes')
const AsistenciaMedica = require('../../Models/Encuestas/tbl_asistencia_medica')


exports.ListarTablas = async (req, res) => {

    try {
      
        const Consulta = await Promise.all([

            
            Covid.find(),
            Obesidad.find(),
            Hipertension.find(),
            Diabetes.find(),
            Depresion.find(),
            AsistenciaMedica.find(),
            

        ])

        res.send({data: Consulta})

    } catch (e) {
        res.status(500).json({error: e})
    }

}