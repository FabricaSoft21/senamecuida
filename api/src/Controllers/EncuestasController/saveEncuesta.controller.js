const encuesta = {}


const encuestaDiabetes = require('../../Models/Encuestas/tbl_diabetes')
const encuestaHipertension = require('../../Models/Encuestas/tbl_hipertension')
const encuestaObesidad = require('../../Models/Encuestas/tbl_obesidad')
const encuestaDepresion = require('../../Models/Encuestas/tbl_depresion')
const encuestaCovid = require('../../Models/Encuestas/tbl_covid')
const encuestaAsistenciaMedica = require('../../Models/Encuestas/tbl_asistencia_medica')


encuesta.saveDiabetes = async (req, res) => {
    const respuestas = req.body.resultado
    const cedula = req.body.cedula


    const newDiabetes = new encuestaDiabetes({
        documentoIdentidad: cedula,
        presionArterial: respuestas[0],
        embarazo: respuestas[1],
        colesterol: respuestas[2],
        exameGlucosa: respuestas[3],
        antecedentes: respuestas[4],
        pariente: respuestas[5]
    })
    await newDiabetes.save()
    console.log('Creado')
}

encuesta.saveHipertension = async (req, res) => {
    const respuestas = req.body.resultado
    const cedula = req.body.cedula

    const newHipertension = new encuestaHipertension({
        documentoIdentidad: cedula,
        ejercicio: respuestas[0],
        presionarterial: respuestas[1],
        diagnosticodiabetes: respuestas[2],
        familiar: respuestas[3],
    })
    await newHipertension.save()
    console.log('Creado')
}

encuesta.saveObesidad = async (req, res) => {
    const respuestas = req.body.resultado
    const cedula = req.body.cedula


    const newObesidad = new encuestaObesidad({
        documentoIdentidad: cedula,
        peso: respuestas[0],
        estatura: respuestas[1],
        ejercicio: respuestas[2],
        comidachatarra: respuestas[3],
        familiar: respuestas[4],
        dieta: respuestas[5]
    })
    await newObesidad.save()
    console.log('Creado')
}

encuesta.saveDepresion = async (req, res) => {
    const respuestas = req.body.resultado
    const cedula = req.body.cedula


    const newDepresion = new encuestaDepresion({
        documentoIdentidad: cedula,
        perdidainteres: respuestas[0],
        altibajos: respuestas[1],
        faltaconcentracion: respuestas[2],
        malpeso: respuestas[3],
        somnolencia: respuestas[4],
        aislamiento: respuestas[5]
    })
    await newDepresion.save()
    console.log('Creado')
}

encuesta.saveCovid = async (req, res) => {
    const respuestas = req.body.resultado
    const cedula = req.body.cedula


    const newCovid = new encuestaCovid({
        documentoIdentidad: cedula,
        fiebre: respuestas[0],
        tos: respuestas[1],
        dolorTragar: respuestas[2],
        malestarGeneral: respuestas[3],
        dificultadRespirar: respuestas[4],
        gripa: respuestas[5],
        diarrea: respuestas[6],
        contactoSospechoso: respuestas[7],
        tratamiento: respuestas[8],
    })
    await newCovid.save()
    console.log('Creado')
}

encuesta.saveAsistenciaMedica = async (req, res) => {
    const respuestas = req.body.resultado
    const cedula = req.body.cedula


    const newAsistenciaMedica = new encuestaAsistenciaMedica({
        nombre: respuestas[0],
        documentoIdentidad: respuestas[1],
        email: respuestas[2],
        telefono: respuestas[3],
        direccionResidencia: respuestas[4],
        eps: respuestas[5],
    })
    await newAsistenciaMedica.save()
    console.log('Creado')
}



module.exports = encuesta;