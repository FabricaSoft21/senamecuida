const pdfCtrl = {}

const Aprendiz = require('../../Models/Aprendiz/tbl_aprendiz');
const Funcionario = require('../../Models/Funcionario/tbl_funcionario');
const Visitante = require('../../Models/Visitante/tbl_visitante');


pdfCtrl.getVisitante = async (req, res) => {
    const visitante = await Visitante.find();
    res.json(visitante)

    console.log('Estas en al back, visitante')
}

pdfCtrl.getFuncionario = async (req, res) => {
    const funcionario = await Funcionario.find();
    res.json(funcionario)

    console.log('Estas en al back, funcionario')

}

pdfCtrl.getAprendiz = async (req, res) => {
    const aprendiz = await Aprendiz.find();
    res.json(aprendiz)

    console.log('Estas en al back, Aprendiz')
}


module.exports = pdfCtrl;