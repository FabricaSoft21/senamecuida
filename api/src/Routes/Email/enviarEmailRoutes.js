const {Router} = require('express');
const router = Router(); 

const {sendDiabetes, sendCovid, sendDepresión, sendHipertensión, sendObesidad} = require('../../Controllers/EmailController/sendEmail.controller')
const {sendDiabetesAp, sendCovidAp,sendDepresiónAp, sendHipertensiónAp,sendObesidadAp} = require('../../Controllers/EmailController/sendEmailAp.controller')

// ENFERMERO

router.route('/diabetes')
    .post(sendDiabetes)

router.route('/hipertension')
    .post(sendHipertensión)

router.route('/obesidad')
    .post(sendObesidad)

router.route('/depresion')
    .post(sendDepresión)

router.route('/covid')
    .post(sendCovid)



// USUARIOS
router.route('/diabetesAp')
    .post(sendDiabetesAp)

router.route('/hipertensionAp')
    .post(sendHipertensiónAp)

router.route('/obesidadAp')
    .post(sendObesidadAp)

router.route('/depresionAp')
    .post(sendDepresiónAp)

router.route('/covidAp')
    .post(sendCovidAp)

    
module.exports = router;