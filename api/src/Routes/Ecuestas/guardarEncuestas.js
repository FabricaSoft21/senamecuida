const {Router} = require('express');
const router = Router(); 

const {saveDiabetes, saveHipertension, saveObesidad,saveDepresion,saveCovid,saveAsistenciaMedica}  = require('../../Controllers/EncuestasController/saveEncuesta.controller')

// ENFERMERO

router.route('/diabetes')
    .post(saveDiabetes)

router.route('/hipertension')
    .post(saveHipertension)

router.route('/obesidad')
    .post(saveObesidad)

router.route('/depresion')
    .post(saveDepresion)

router.route('/covid')
    .post(saveCovid)

router.route('/asistenciaMedica')
    .post(saveAsistenciaMedica)


    
module.exports = router;