const {Router} = require('express');
const router = Router();

const { getAprendiz, getFuncionario, getVisitante } = require('../../Controllers/Pdf/pdfController')

router.route('/visitante')
        .get(getVisitante)

router.route('/funcionario')
        .get(getFuncionario)

router.route('/aprendiz')
        .get(getAprendiz)

module.exports = router;